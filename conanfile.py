from conans import ConanFile, CMake, tools

required_conan_version = ">=1.33.0"

class LexborConan(ConanFile):
    name = "lexbor"
    license = "Apache-2.0"
    homepage = "https://github.com/lexbor/lexbor/"
    url = "https://github.com/conan-io/conan-center-index"
    description = "Lexbor is development of an open source HTML Renderer library"
    topics = ("html5", "css", "parser", "renderer")
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    exports_sources = ["CMakeLists.txt", ]
    options = {
        "shared": [True, False], 
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False, 
        "fPIC": True,
    }

    _cmake = None

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC
        del self.settings.compiler.cppstd
        del self.settings.compiler.libcxx

    def source(self):
        tools.get(**self.conan_data["sources"][self.version],
            destination=self._source_subfolder, strip_root=True)

    def _configure_cmake(self):
        if self._cmake:
            return self._cmake
        self._cmake = CMake(self)
        self._cmake.definitions["LEXBOR_BUILD_SHARED"] = self.options.shared
        self._cmake.definitions["LEXBOR_BUILD_STATIC"] = not self.options.shared
        self._cmake.definitions["LEXBOR_TESTS_CPP"] = False
        self._cmake.definitions["LEXBOR_BUILD_SEPARATELY"] = False
        self._cmake.definitions["LEXBOR_INSTALL_HEADERS"] = True
        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src=self._source_subfolder)
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        if self.options.shared:
            self.cpp_info.libs = ["lexbor"]
        else:
            self.cpp_info.libs = ["lexbor_static"]
